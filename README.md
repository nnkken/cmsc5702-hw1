# Amazon eBay Book Searcher
**Amazon eBay Book Searcher** is a book searching platform, getting books from eBay and Amazonm, performing price comparison and books recommendation.
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Amazon eBay Book Searcher](#amazon-ebay-book-searcher)
	- [Installation](#installation)
				- [Requirement](#requirement)
	- [Front End Development](#front-end-development)
				- [Requirement](#requirement)
	- [Design Architecture](#design-architecture)
			- [main.py](#mainpy)
			- [amazon.py](#amazonpy)
			- [ebay.py](#ebaypy)
			- [Borrowed codes](#borrowed-codes)
	- [Backend - Main Program](#backend-main-program)
			- [HTTP Webserver](#http-webserver)
			- [Asynchronous Query](#asynchronous-query)
	- [Backend - Amazon Searcher](#backend-amazon-searcher)
			- [API Request](#api-request)
			- [API Response](#api-response)
			- [Remarks](#remarks)
	- [eBay Searcher](#ebay-searcher)
			- [API Request](#api-request)
			- [API Response](#api-response)
			- [Remarks](#remarks)
	- [User Interface](#user-interface)
			- [Search by keyword/IBSN](#search-by-keywordibsn)
			- [Change the currency](#change-the-currency)
			- [Search for similar products](#search-for-similar-products)

<!-- /TOC -->


<!-- Installation and Development-->
## Installation
##### Requirement
- Python (either 2 or 3)

1.  `python main.py`
2. Go to <http://localhost:8080/static/index.html>

## Front End Development
##### Requirement
- NodeJS
- NPM
1. `cd ui`
2. `npm install`
3. `npm run build`
<!-- /Installation and Development-->

<!-- Design Architecture -->
## Design Architecture
The whole system is written in Python. It includes the following components.
![images](./images/big_pic.jpg)

#### main.py
This program is responsible for starting and hosting the front-end UI, and controlling the thread pool for asynchronous query to Amazon and eBay. Each thread correspond to one query.

#### amazon.py
This program is responsible for preparing a RESTful URL with signature based on the searching parameters from main.py, fetching the search results from Amazon RESTful API, and parsing the resulting XML to return the required data to main.py.

#### ebay.py
This program is responsible for preparing a RESTful URL with signature based on the searching parameters from main.py, fetching the search results from eBay RESTful API, and parsing the resulting JSON to return the required data to main.py.

#### Borrowed codes
We have borrowed some libraries from the Internet.

For the server part, we used **Bottle** (bottle.py) as web framework and urlfetch (urlfetch.py) as HTTP client library.

For the UI part, we used **Vue.js** as frontend framework, and also some polyfills for browser capabilities.
The borrowed codes of the UI part, together with their own dependencies and build tools, are inside ui/node_modules, which appears after running `npm install`.
<!-- /Design Architecture -->

<!-- Backend - Main Program-->
## Backend - Main Program
#### HTTP Webserver
Our main program hosts front-end UI and provides keyword searching as HTTP GET Methods.

The HTTP Webservice functionality is provided by the **Bottle** framework. Which we can see in the ***main.py***, every URI is allocated to a python function,

Upon user access *http://localhost:8080/static/index.html*, our main program with return the static html file with all the javascript to generate the front-end UI on the client side.

Upon user click the search button in the UI, a GET method carrying the keyword as *http://localhost:8080/query/\$keyword* or *http://localhost:8080/query_isbn/\$isbn_code* is sent to the webserver, functions in amazon.py and ebay.py will be triggered as async functions to collect data.

Returned response data are reformated as JSON, and handled by the front-end framework to display the searched items.
#### Asynchronous Query
To enhance the user experience, we created 30 threadpool in the main program to perform asynchronize program call on the Amazon Searcher (amazon.py) and eBay Searcher (ebay.py)

The functionality is provided by the standard python module **Multiprocessing**.

It provides a **thread-based Pool interface** by importing
```python
from multiprocessing.pool import ThreadPool
```
The idea is that it use a dummy Process class wrapping python threads. And we use function `apply_async` to ensure the threads run in parallel. Detailed implementation can be found in the query functions in ***main.py***.
<!-- /Backend - Main Program-->


<!-- Backend - Amazon Searcher-->
## Backend - Amazon Searcher
Amazon searcher mainly communicate with the main program to get the keyword, and calling the **ItemSearch** method provided by Amazon to get the data. (http://docs.aws.amazon.com/AWSECommerceService/latest/DG/ItemSearch.html)

#### API Request
The webservice is also a RESTful API in HTTP GET method and all the parameters (such as: *Keywords*, *ResponseGroup*, *Sort*, *ServiceName*, ...) are passed to the AWS service in form of query parameters. (example request can be viewed in the AWS site above)

Note that to access the service we have to Signup as a developer, and the account credentials like secret keys are encrypted in SHA256 and also passed as a query parameter.

The url is sent with response collected by the **UrlFetch** module of python.
#### API Response
By default successful Webservice Response are in form of below XML. (example response can be viewed in the AWS site above)

We then obtained the desired information such as Item name, Item page url, Price and Currency by extracting the XML based on its defined structure.

The extracted data are stored as python dictionary, as passed to main program to be converted as JSON.

#### Remarks
Note that as we are performing both **keyword-based** and **ISBN-based** search, we have 2 functions with different pre-defined search id type (**keyword** or **ISBN*) in 2 sets of webservice requests.

And for the **Search similar product** functionality. The similar products IDs are also included in this webservice response. Which will be ISBN parameter input in our **Search similar product** function.
<!-- /Backend - Amazon Searcher-->


<!-- Backend - eBay Searcher-->
## eBay Searcher
eBay searcher mainly communicate with the main program to get the keyword, and calling the **findItemsAdvanced** (http://developer.ebay.com/Devzone/finding/CallRef/findItemsAdvanced.html), **findItemsByProduct** (http://developer.ebay.com/Devzone/finding/CallRef/findItemsByProduct.html) and **getProductDetails** (http://developer.ebay.com/Devzone/product/CallRef/getProductDetails.html) methods provided by eBay to get the data.


#### API Request
The webservice is also a RESTful API in HTTP GET method and all the parameters (such as: *Keywords*, *ResponseGroup*, *Sort*, *ServiceName*, ...) are passed to the eBay service in form of query parameters. (example request can be viewed in the eBay site above)

Note that to access the service we have to Signup as a developer, and the account credentials like SECURITY-APPNAME are also included in the parameters.

The url is sent with response collected by the **UrlFetch** module of python.
#### API Response
By default successful Webservice Response are in form of below XML. (example response can be viewed in the AWS site above)

We then obtained the desired information such as Item name, Item page url, Price and Currency by extracting the XML based on its defined structure.

The extracted data are stored as python dictionary, as passed to main program to be converted as JSON.

#### Remarks
Note that as we are performing both **keyword-based** and **ISBN-based** search, we have 2 functions with different pre-defined search id type (**keyword** or **ISBN*) in 2 sets of webservice requests.

And for the **Search similar product** functionality. The similar product ids are coming from AWS Webservice, and the key ids and searched again in eBay.
<!-- /Backend - eBay Searcher-->

<!-- UI-->
## User Interface
From the perspective of UI, we mainly provides 3 functionality:
![images](./images/front_001.png)
1. Search by keyword/ISBN
2. Change the currency
3. Search for similar products

#### Search by keyword/IBSN
By switching the radio button as **Keyword** and **ISBN**, we support both book searching by keyword and ISBN:
- Keyword
![images](./images/front_002_keyword.png)
- ISBN
![images](./images/front_003_isbn.png)
And as we can see the above pictures, for items where both eBay and Amazon provides, both item information are shown in the same column, while the cheaper price will be shown in the first position, with a larger font-size..

#### Change the currency
By switching the dropdown button of currency in the UI, we can make us of the currency data stored in the javascript to produce real-time change on the items' price.
![images](./images/front_004_currency.png)

#### Search for similar products
For items with similar product ids provided from the Amazon webservice, A **Search similar product** button is shown and we can make use of them to search for similar products.
Below is an example of similar products suggested based on the book *Python Parallel Programming Cookbook*.
![images](./images/front_006_similar.png)
<!--/UI-->
