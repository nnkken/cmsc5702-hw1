import Promise from "promise-polyfill";
import "whatwg-fetch";
import Vue from "vue";
import Set from "es6-set";

if (!window.Promise) {
    window.Promise = Promise;
}

new Vue({
    el: "#app",
    data: {
        queryString: "",
        items: [],
        itemVersion: 0,
        queryMode: "keyword",
        currency: "USD",
        CURRENCIES: {
            HKD: 1,
            USD: 7.7603,
            CNY: 1.1293,
            EUR: 8.2268,
            JPY: 0.0678,
            GBP: 9.6720,
            AUD: 5.9729,
            NZD: 5.5888,
            CAD: 5.9335,
            TWD: 0.2518,
            KRW: 0.0068,
            THB: 0.2201,
            SGD: 5.4635,
        }
    },
    methods: {
        setItems: function(items) {
            const knownCurrencies = Object.keys(this.CURRENCIES);
            const knownCurrencyItems = items.filter((item) => knownCurrencies.includes(item.currency));
            items = this.showCheapestEbayItem(knownCurrencyItems);
            items = this.concatEbayAndAmazon(items);
            items = this.tagByPrice(items);
            const sortedItems = items.sort((item1, item2) => {
                item1 = item1[item1.sortedTags[0]];
                item2 = item2[item2.sortedTags[0]];
                const price1 = this.convertCurrency(
                    item1.currency, "HKD", item1.price
                );
                const price2 = this.convertCurrency(
                    item2.currency, "HKD", item2.price
                );
                return price1 - price2;
            });
            this.items = sortedItems;
        },
        submit: function(e) {
            this.queryString = this.queryString.trim();
            if (this.queryString !== "") {
                this.query(this.queryString, this.queryMode);
            }
            e.preventDefault();
        },
        query: function(queryString, queryMode) {
            const endpoints = {
                ISBN: "query_isbn",
                keyword: "query"
            };
            const endpoint = endpoints[queryMode];
            this.itemVersion += 1;
            const versionWhenSubmit = this.itemVersion;
            fetch(`/${endpoint}/${queryString}`)
                .then((response) => response.json())
                .then((json) => {
                    if (this.itemVersion <= versionWhenSubmit) {
                        this.setItems(json.items);
                    }
                })
                .catch((err) => {
                    console.error(err);
                    // TODO error handling, e.g. show msg in UI?
                });
        },
        searchSimilar: function(item) {
            const isbns = item.similarProducts.join(",");
            this.query(isbns, "ISBN");
        },
        convertCurrency: function(from, to, value) {
            if (value === 0) {
                return 0;
            }
            return value * this.CURRENCIES[from] / this.CURRENCIES[to];
        },
        renderPrice: function(currency, value) {
            if (!currency || !value) {
                return "";
            }
            if (!(Object.keys(this.CURRENCIES)).includes(currency)) {
                return `${currency} ${value}`;
            }
            value = this.convertCurrency(currency, this.currency, value);
            return `${this.currency} ${value.toFixed(2)}`;
        },
        showCheapestEbayItem: function(items) {
            var idSet = new Set();
            var result = [];
            for (var i = 0; i < items.length; i++) {
                // for eBay item, only keep the cheapest record
                if (items[i].tag === "eBay") {
                    if (!idSet.has(items[i].epid)) {
                        result.push(items[i]);
                        idSet.add(items[i].epid);
                    }
                } else {
                    result.push(items[i]);
                }
            }
            return result;
        },
        ifExist: function(url, tag) {
            if (!url) {
                return "";
            }
            if (tag === "eBay") {
                return "(from eBay)";
            }
            return "(from Amazon)";
        },
        concatEbayAndAmazon: function(items) {
            var idSet = {};
            var result = [];
            var j = 0;
            for (var i = 0; i < items.length; i++) {
                // concat item based on isbn
                const tag = items[i].tag;
                const itemInfo = {
                    price: items[i].price,
                    productUrl: items[i].productUrl,
                    currency: items[i].currency
                };
                items[i][tag] = itemInfo;
                if (!(items[i].isbn in idSet)) {
                    idSet[items[i].isbn] = j;
                    result.push(items[i]);
                    j++;
                } else {
                    const prev = result[idSet[items[i].isbn]];
                    if (!prev[tag]) {
                        prev[tag] = itemInfo;
                    } else {
                        const prevPrice = this.convertCurrency(
                            prev[tag].currency, "HKD", prev[tag].price
                        );
                        const currPrice = this.convertCurrency(
                            items[i].currency, "HKD", items[i].price
                        );
                        if (currPrice < prevPrice) {
                            prev[tag] = itemInfo;
                        }
                    }
                    if (items[i].similarProducts) {
                        prev.similarProducts = items[i].similarProducts;
                    }
                }
            }
            return result;
        },
        tagByPrice: function(items) {
            const TAGS = ["eBay", "Amazon"];
            items.forEach((item) => {
                const sortedTags = TAGS.filter((t) => item[t]).sort((t1, t2) => {
                    const price1 = this.convertCurrency(
                        item[t1].currency, "HKD", item[t1].price
                    );
                    const price2 = this.convertCurrency(
                        item[t2].currency, "HKD", item[t2].price
                    );
                    return price1 - price2;
                });
                item.sortedTags = sortedTags;
            });
            return items;
        }
    }
});
