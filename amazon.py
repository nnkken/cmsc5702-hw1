import base64
import time
import collections
import hashlib
import hmac
import re
import urlfetch
import xml.etree.ElementTree as ET
try:
	from urllib import quote
except:
	from urllib.parse import quote

def sign(msg):
	aws_access_key_id = "AKIAJYAE5ULCQ5RAAA6A".encode("UTF-8")
	aws_secret_key = "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".encode("UTF-8")
	msg = msg.encode("UTF-8")
	digest = hmac.new(aws_secret_key, msg, hashlib.sha256).digest()
	b64 = base64.b64encode(digest).strip()
	return b64.decode("ASCII")

# Prepare the RESTful URL
# (Required) keyword - The keyword to search, which can be a ASIN or ISBN also
# (Required) searchBy - Can be of value either "keyword", "ASIN", or "ISBN"
def prepareURL(keyword, searchBy):
	# Set the region to US Amazon
	endpoint = "webservices.amazon.com"

	uri = "/onca/xml"
	time.ctime()

	if searchBy == "keyword":
		params = {
			"Timestamp": time.strftime('%Y-%m-%dT%H:%M:%S.000Z'),
			"Service": "AWSECommerceService",
			"Operation": "ItemSearch",
			"AWSAccessKeyId": "AKIAJYAE5ULCQ5RAAA6A",
			"AssociateTag": "fortesit-20",
			"SearchIndex": "Books",
			"Keywords": keyword,
			"ResponseGroup": "Images,ItemAttributes,OfferSummary,Similarities,Small",
			"Sort": "salesrank",
		}
	elif searchBy == "ASIN" or searchBy == "ISBN":
		params = {
			"Timestamp": time.strftime('%Y-%m-%dT%H:%M:%S.000Z'),
			"Service": "AWSECommerceService",
			"Operation": "ItemLookup",
			"AWSAccessKeyId": "AKIAJYAE5ULCQ5RAAA6A",
			"AssociateTag": "fortesit-20",
			"ItemId": keyword,
			"IdType": searchBy,
			"ResponseGroup": "Images,ItemAttributes,OfferSummary,Similarities,Small",
			"Sort": "salesrank",
		}
		if searchBy == "ISBN":
			params["SearchIndex"] = "Books"
	else:
		print("Unsupport search type: " + searchBy)
		return None

	# Sort the parameters by key
	params = collections.OrderedDict(sorted(params.items()))

	pairs = []
	for key, value in params.items():
		pairs.append(quote(key) + "=" + quote(value))

	# Generate the canonical query
	canonical_query_string = "&".join(map(str, pairs))

	# Generate the string to be signed
	string_to_sign = "GET\n" + endpoint + "\n" + uri + "\n" + canonical_query_string

	# Generate the signature required by the Product Advertising API
	signature = sign(string_to_sign)

	# Generate the signed URL
	request_url = 'http://' + endpoint + uri + '?' + canonical_query_string + '&Signature=' + quote(signature)

	return request_url

def xmlParse(url):
	root = ET.fromstring(urlfetch.get(url).content)
	namespaces = {"ecs": "http://webservices.amazon.com/AWSECommerceService/2011-08-01"}
	items = []
	for itemsXml in root.findall("ecs:Items", namespaces):
		for item in itemsXml.findall("ecs:Item", namespaces):
			asin = item.find("ecs:ASIN", namespaces).text
			name = item.find("ecs:ItemAttributes/ecs:Title", namespaces).text
			isbn = item.find("ecs:ItemAttributes/ecs:ISBN", namespaces)
			if isbn is not None:
				isbn = isbn.text
			galleryUrl = item.find("ecs:MediumImage/ecs:URL", namespaces)
			if galleryUrl is None:
				galleryUrl = "https://images-na.ssl-images-amazon.com/images/G/01/x-site/icons/no-img-sm._V192198896_BO1,204,203,200_.gif"
			else:
				galleryUrl = galleryUrl.text
			productUrl = item.find("ecs:DetailPageURL", namespaces).text
			currency = item.find("ecs:OfferSummary/ecs:LowestNewPrice/ecs:CurrencyCode", namespaces)
			price = item.find("ecs:OfferSummary/ecs:LowestNewPrice/ecs:FormattedPrice", namespaces)
			if price is not None:
				currency = currency.text
				price = price.text
				price = re.sub("[^0-9.]", "", price)
				price = float(price)
			similarProducts = []
			for similarProductsXml in item.findall("ecs:SimilarProducts", namespaces):
				for similarProductXml in similarProductsXml.findall("ecs:SimilarProduct", namespaces):
					similarProducts.append(similarProductXml.find("ecs:ASIN", namespaces).text)
			items.append({
				"asin": asin,
				"name": name,
				"isbn": isbn,
				"galleryUrl": galleryUrl,
				"productUrl": productUrl,
				"currency": currency,
				"price": price,
				"similarProducts": similarProducts,
				"tag": "Amazon"
			})

			# Debug message
			# print "==============================="
			# print "ASIN: " + asin
			# print "Title: " + name
			# print "ISBN10: " + isbn
			# print "Image: " + galleryUrl
			# print "Item page: " + productUrl
			# print "Price: " + currency + str(price)
			# print "Similar Products (ASIN): "
			# print similarProducts
	return items

# Start query
# (Required) keyword - The keyword to search, which can be a ASIN or ISBN also
# (Required) searchBy - Can be of value either "keyword", "ASIN", or "ISBN"
def query(keyword, searchBy):
	return xmlParse(prepareURL(keyword, searchBy))

# Example use
# query("Automata Theory", "keyword")
# query("0321455363", "ISBN")
# query("113318779X", "ASIN")
