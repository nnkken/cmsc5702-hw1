import urlfetch
import json
from multiprocessing.pool import ThreadPool


pool = ThreadPool(20)


FINDING_API = "http://svcs.ebay.com/services/search/FindingService/v1"
PRODUCT_API = "http://svcs.ebay.com/services/marketplacecatalog/\
ProductService/v1"


def eBayCall(endpoint, opName, payload):
    params = {
        "OPERATION-NAME": opName,
        "REQUEST-DATA-FORMAT": "JSON",
        "RESPONSE-DATA-FORMAT": "JSON",
        "SECURITY-APPNAME": "ChunChun-2017CMSC-PRD-82466ad41-7348c571"
    }
    payloadJson = json.dumps(payload)
    r = urlfetch.post(endpoint, params=params, data=payloadJson)
    return r.json


def eBayGetIsbn(epids):
    if isinstance(epids, str) or isinstance(epids, int):
        epids = [epids]
    detailRequests = [
        {
            "productIdentifier": {
                "ePID": epid
            }
        }
        for epid in epids
    ]
    payload = {
        "productDetailsRequest": detailRequests
    }
    return eBayCall(PRODUCT_API, "getProductDetails", payload)


def eBayFindProductByIsbn(isbn):
    payload = {
        "productId": {
            "@type": "ISBN",
            "__value__": isbn
        }
    }
    return eBayCall(FINDING_API, "findItemsByProduct", payload)


def eBaySearchRaw(keyword, limit=20):
    CATEGORY_BOOKS = "267"
    payload = {
        "categoryId": CATEGORY_BOOKS,
        "keywords": keyword,
        "paginationInput": {
            "entriesPerPage": limit
        }
    }
    return eBayCall(FINDING_API, "findItemsAdvanced", payload)


def jsonGetPath(item, *path):
    for nodeName in path:
        if item is None:
            return None
        try:
            item = item[nodeName]
        except (KeyError, IndexError):
            return None
    return item


def getIsbnFromEpidsPartial(epidsPart):
    json = eBayGetIsbn(epidsPart)
    products = jsonGetPath(json, "getProductDetailsResponse", 0, "product")
    if not products:
        return None
    isbns = {}
    for product in products:
        epid = jsonGetPath(product, "productIdentifier", 0, "ePID", 0)
        if not epid:
            continue
        details = jsonGetPath(product, "productDetails")
        if not details:
            continue
        isbn = [
            jsonGetPath(d, "value", 0, "text", 0, "value", 0)
            for d in details
            if jsonGetPath(d, "propertyName", 0) == "ISBN"
        ]
        if not isbn:
            continue
        isbns[epid] = isbn[0]
    return isbns


def getIsbnsFromEpids(epids):
    asyncJobs = []
    for i in range(0, len(epids), 10):
        epidsPart = epids[i: i + 10]
        asyncJob = pool.apply_async(getIsbnFromEpidsPartial, (epidsPart,))
        asyncJobs.append(asyncJob)
    isbnParts = []
    for asyncJob in asyncJobs:
        isbn = asyncJob.get()
        isbnParts.append(isbn)
    isbnResult = {}
    for isbn in isbnParts:
        isbnResult.update(isbn)
    return isbnResult


def getQueryResult(json, rootName):
    searchResult = jsonGetPath(json, rootName, 0, "searchResult", 0, "item")
    if not searchResult:
        return []
    items = []
    for item in searchResult:
        name = jsonGetPath(item, "title", 0)
        epid = jsonGetPath(item, "productId", 0, "__value__")
        if not epid:
            continue
        galleryUrl = jsonGetPath(item, "galleryURL", 0)
        productUrl = jsonGetPath(item, "viewItemURL", 0)
        priceInfo = jsonGetPath(item, "sellingStatus", 0, "currentPrice", 0)
        currency = priceInfo["@currencyId"]
        price = float(priceInfo["__value__"])
        items.append({
            "name": name,
            "epid": epid,
            "galleryUrl": galleryUrl,
            "productUrl": productUrl,
            "currency": currency,
            "price": price,
            "tag": "eBay"
        })
    return items


def queryByIsbn(isbn):
    isbn = str(isbn)
    json = eBayFindProductByIsbn(isbn)
    items = getQueryResult(json, "findItemsByProductResponse")
    for item in items:
        item["isbn"] = isbn
    return items


def query(keyword):
    json = eBaySearchRaw(keyword)
    items = getQueryResult(json, "findItemsAdvancedResponse")
    epids = set()
    for item in items:
        epids.add(item["epid"])
    epids = list(epids)
    epidToIsbn = getIsbnsFromEpids(epids)
    for item in items:
        epid = item["epid"]
        if epid in epidToIsbn:
            item["isbn"] = epidToIsbn[epid]
    return items
