from multiprocessing.pool import ThreadPool
from bottle import get, redirect, static_file, run
import ebay
import amazon

pool = ThreadPool(30)


@get("/")
def root_get():
    return redirect("/static/index.html")


@get("/static/<path>")
def static_get(path):
    return static_file(path, root="ui/dist")


def mergeItems(*itemsList):
    merged = []
    for items in itemsList:
        merged.extend(items)
    return merged


@get("/query_isbn/<isbns>")
def queryIsbn(isbns):
    try:
        asyncJobs = []
        isbns = isbns.split(",")
        for isbn in isbns:
            ebayAsync = pool.apply_async(ebay.queryByIsbn, (isbn,))
            amazonAsync = pool.apply_async(amazon.query, (isbn, "ISBN"))
            asyncJobs.append(ebayAsync)
            asyncJobs.append(amazonAsync)
        items = []
        for asyncJob in asyncJobs:
            items.append(asyncJob.get())
        items = mergeItems(*items)
        return {"items": items}
    except:
        # TODO error handling
        raise


@get("/query/<keyword>")
def query(keyword):
    try:
        ebayAsync = pool.apply_async(ebay.query, (keyword,))
        amazonAsync = pool.apply_async(amazon.query, (keyword, "keyword"))
        ebayItems = ebayAsync.get()
        amazonItems = amazonAsync.get()
        items = mergeItems(ebayItems, amazonItems)
        return {"items": items}
    except:
        # TODO error handling
        raise


run(host="localhost", port=8080)
